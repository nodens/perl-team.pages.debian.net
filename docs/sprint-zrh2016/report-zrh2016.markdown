Debian Perl Sprint 2016
=======================

Introduction
------------

6 members of the Debian Perl Group met in Zurich over the weekend
from May 19 to May 22 2016 to continue the development around perl
for Stretch and to work on QA tasks across our 3000+ packages. The
preparation details can be found on the [sprint wiki][].

The participants would like to thank [ETH Zurich][] for hosting us,
and all donors to the Debian project who helped to cover a large
part of our expenses.

[sprint wiki]: https://wiki.debian.org/Sprints/2016/DebianPerlSprint
[ETH Zurich]: https://www.ethz.ch/en.html


Bugs and Uploads
================

Overview
--------

Bugs [tagged][] with:

* user: debian-perl@lists.debian.org
* usertags: zrh2016

A total of 36 bugs were filed/worked on. These include:

* newly filed: 16 bugs
    - incl. 6 fixed during the sprint
    - incl. 4 RM bugs to drop unmaintained upstream software
* resolved: 12 bugs
    - incl. 6 bugs filed during the sprint

A total of 28 accepted uploads to team-maintained packages were made
at the sprint.

[tagged]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=zrh2016;users=debian-perl@lists.debian.org

Details of fixes and uploads
----------------------------

Below is a selection of bugs worked on during the sprint. Some items
are covered in more detail below.

* [#823217][]: libmouse-perl: Mouse generates a 'No package name
  defined' under ModPerl::Registry
    - Reproduced and tagged 'confirmed', no progress.
* [#787500][]: libcoro-perl: FTBFS with perl 5.22
    - Fixed in git and uploaded using an anonymous patch submission.
    - Made clear that this is not supported upstream.
* [#823437][]: lintian: Relax regexp for missing-dependency-on-perlapi
    - pending
* [#824938][]: debsums: debsums no longer ignores dpkg-path-excluded
  files
    - Reassigned to dpkg-dev by Guillem.
* [#818678][]: mb2md: Unable to find target mailbox directory
    - Fixed and a test suite for it written.
* [#823294][]: libapp-cpants-lint-perl: cpants-lints: fails to install
    - Fixed by adding missing Breaks/Replaces.

[#823217]: https://bugs.debian.org/823217
[#787500]: https://bugs.debian.org/787500
[#823437]: https://bugs.debian.org/823437
[#824938]: https://bugs.debian.org/824938
[#818678]: https://bugs.debian.org/818678
[#823294]: https://bugs.debian.org/823294

Bugs related to autopkgtest failures
------------------------------------

* [#806542][]: liblinux-prctl-perl: skip the autopkgtests for now
* ci.debian.net is testing arm64 now, reviewed the failures:
    - [#824839][]: librrd-simple-perl: FTBFS on armhf and arm64:
      t/23graph.t failures
    - [#824843][]: libsys-syscall-perl: FTBFS on arm64: test suite
      failures
    - [#824844][]: libsys-cpu-perl: autopkgtest failures on arm64
    - [#824845][]: libdanga-socket-perl: autopkgtest failures on arm64

[#806542]: https://bugs.debian.org/806542
[#824839]: https://bugs.debian.org/824839
[#824843]: https://bugs.debian.org/824843
[#824844]: https://bugs.debian.org/824844
[#824845]: https://bugs.debian.org/824845

Miscellaneous package work
==========================

* Checked for packages which don't have the group as maintainer,
  and fixed them.
* Updated several packages to new upstream releases.
* Minor maintenance changes to debsums were performed.
* Other QA: Packages were removed from unstable:
    - libfile-scan-perl ([#824860][])
    - perlipq ([#824861][])
    - libzeromq-perl and ([#824905][])
    - libdist-zilla-plugin-podspellingtests-perl ([#786749][]) was
      retitled to RM (ROM; obsoleted by
      libdist-zilla-plugin-test-podspelling-perl)
* More removals were also proposed:
    - libpoe-api-peek-perl([#789381][])
    - libdevel-findref-perl ([#787446][])
    - libxml-easyobj-perl ([#797289][])
* Commented on [#824696][]: debhelper: "dh_perl should report perl:any"
* Some work was done to fix or work around nondeterministic test
  failures as detected by the [reproducible build project][].

[#824860]: https://bugs.debian.org/824860
[#824861]: https://bugs.debian.org/824861
[#824905]: https://bugs.debian.org/824905
[#786749]: https://bugs.debian.org/786749
[#789381]: https://bugs.debian.org/789381
[#787446]: https://bugs.debian.org/787446
[#797289]: https://bugs.debian.org/797289
[#824696]: https://bugs.debian.org/824696
[reproducible build project]: https://tests.reproducible-builds.org/unstable/amd64/pkg_set_maint_pkg-perl-maintainers.html

Fixes prompted by DUCK reports
------------------------------

A mass-commit was performed to replace http URLs with https in
`debian/copyright` and `debian/upstream/metadata`. Additionally,
dh-make-perl and libconfig-model-dpkg-perl were updated to use https
in the first place.

Perl 5.24 transition planning/packaging
=======================================

Perl 5.24 was released in May 2016, and it was confirmed that the plan
was to get this into stretch before freeze, based on transition freeze
on 5th November 2016, soft freeze on 5th January 2017, freeze on 5th
February 2017.

The transition was expected to be fairly straightforward. Aside from
FTBFS bugs caught by rebuilds, the *rename* package deprecation and
and other syntax deprecations in Perl needed to be caught through log
scanning.

Setting up a test rebuild server
--------------------------------

Work at the sprint mainly focused on getting a more reliable and useful
[test rebuild infrastructure][] up and running. This used debomatic with
some local patches to integrate reprepro and other necessary changes.
This can act as a generic test infrastructure which is planned to be used
for other tests (eg testing against the version of libtest-simple-perl
currently, which will be useful for upstream toolchain maintainers)

During the sprint, the first [transition bugs][] (build failures in XS
modules and packages linking against libperl) were filed:

A [status report][] was sent to the mailing list after the sprint with
more details and future plans.

[test rebuild infrastructure]: http://perl.debian.net/
[transition bugs]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=perl-5.24-transition;users=debian-perl@lists.debian.org
[status report]: https://lists.debian.org/debian-perl/2016/05/msg00021.html

Cross building XS modules
=========================

Niko gave a demo of cross building liblocale-gettext-perl with minimal
packaging changes. This builds on infrastructure provided by the perl
package [#717433][] and an older approach in liblocale-gettext-perl
[#633949][]. The participants then discussed the conditions where this
is viable (i.e. simple enough build dependencies, in particular not
including any Architecture:any Perl modules). The general case is still
unsolved because of the "multi arch interpreter issue" (see for instance
[#717882][]).

A prerequisite is to annotate test build dependencies with the build
profile `<!nocheck>`. It was tentatively agreed that the team should
start doing this. It would be good to derive the annotations from
upstream information where available (cf. the [CPAN::Meta::Spec][]
for upstream test requirements), but this avenue still needs further
investigation.

An updated liblocale-gettext-perl package was uploaded as a test case
for the above. This package is a good candidate for cross building as
it's dependency of debconf-i18n and therefore one of the lowest in the
dependency stack.

[#717882]: https://bugs.debian.org/717882
[#633949]: https://bugs.debian.org/633949
[#717433]: https://bugs.debian.org/717433
[CPAN::Meta::Spec]: https://metacpan.org/pod/CPAN::Meta::Spec#PREREQUISITES

Improvements to team packaging tools
====================================

mr checkout vs. gbp pull
------------------------

When `mr checkout` was used (usually through `mr up`), it generated the
file `debian/upstream/metadata` for newly checked out repositories. This
file is used from pkg-perl-tools scripts, so it made sense to create the
file beforehand if someone was going to work on that repository.

On the other hand, `gbp pull` aborts on unclean working directories, so
the next time that `mr up` would be used on that repository an error was
raised complaining about untracked `debian/upstream/metadata`.

This was an issue for people that usually check out the whole pkg-perl
list of repositories, and it was addressed at the sprint.

dh-make-perl
------------

Bugs were triaged, patches were applied, and fixes were proposed:

* [#813766][]: dh-make-perl: add --force-depends option
* [#815390][]: dh-make-perl: Fail to build package if run with sudo
  ($orig_pwd not initialized)
* [#788198][]: dh-make-perl locate Module::Pluggable::Object falsely
  thinks it is in core
* [#823708][]: dh-make-perl: parse failure if d/control contains
  comments
* [#823067][]: dh-make-perl: please split Debian::Control into a
  separate binary package

[#813766]: https://bugs.debian.org/813766
[#815390]: https://bugs.debian.org/815390
[#788198]: https://bugs.debian.org/788198
[#823708]: https://bugs.debian.org/823708
[#823067]: https://bugs.debian.org/823067

Weak checksums
--------------

Some lintian warnings were noticed for source packages with weak
checksums (e.g. libclass-default-perl_1.51-2) so [#824916][] was
filed to propose a new lintian tag to record the list of affected
packages.

[#824916]: https://bugs.debian.org/824916

pkg-perl-tools
--------------

Both `dpt-forward-patch` and `dpt-forward-bug` were merged into
`dpt-forward`, adopting their mail bug reporting interface. Now
`dpt-forward` also reads `debian/upstream/metadata` and there is a
new branch to create pull requests instead of forwarding patches and
links in comments to GitHub issues.

A new feature was added to `dpt-upstream-repo` and `dpt-debian-upstream`
that will switch to secure URLs where needed.

Some minor improvements were performed in `examples/check-build`, TODO
items were reviewed and updated, and pkg-perl-tools 0.30 version was
uploaded.


YAPC::EU talk on downstream distributions
=========================================

Alex proposed that he present a talk at the next YAPC::EU on the
topic of downstream distribution aimed at CPAN authors, which has
already been [accepted][].

It was agreed that he would based it on gregor's [presentation][] from
Barcelona Perl Workshop 2014.

Discussion on how to make interaction between upstreams and Debian
easier included the suggestion to make it easy for upstream to subscribe
to package bug mail, and handling upstreams which don't have an interest
in receiving bugs/patches from distros (should a disclaimer be added to
the package description? Should patches and bugs not be forwarded?)

[accepted]: http://act.yapc.eu/ye2016/talk/6770
[presentation]: https://pkg-perl.alioth.debian.org/docs/barcelona-perl-workshop-2014/from_debian_with_love.pdf
