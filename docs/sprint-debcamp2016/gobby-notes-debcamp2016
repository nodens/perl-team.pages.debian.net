Debian Perl Group Sprint @ DebCamp 2016
=======================================

Preparation
-----------
https://wiki.debian.org/Sprints/2016/DebianPerlDebCamp
and other linked pages

Thu, 2016-06-23
---------------
Participants: -
Topics:

Fri, 2016-06-24
---------------
Participants: -
Topics:

Sat, 2016-06-25
---------------
Participants: -
Topics:

Sun, 2016-06-26
---------------
Participants: intrigeri, gregoa
Topics:

Mon, 2016-06-27
---------------
Participants: intrigeri, bremner, gregoa
Topics:

Tue, 2016-06-28
---------------
Participants: -
Topics:

Wed, 2016-06-29
---------------
Participants: carnil, bremner, intrigeri, gregoa
Topics discussed: dh-make-perl split, Files-Excluded, reproducible builds, 5.24, gcc6

Thu, 2016-06-30
---------------
Participants: intrigeri, carnil, gregoa
Topics discussed: gcc6, FTBFS, contributors ping, dpt-upstream-repo and .mrconfig

Fri, 2016-07-01
---------------
Participants: -
Topics:
Report → see below


REPORT
======

QA tasks
--------

* Subscribe our Launchpad team, ~pkg-perl-maintainers, to all bugs concerning packages we maintain.
* Git repos cleanup (propose to remove packages from Git that were injected but never finished for upload).
* Drop Mouse deprecation from our TODO list: it's actually still somewhat maintained,
  and lots of packages still use it.
* Go through our RC bugs and propose package removals: https://lists.debian.org/debian-perl/2016/06/msg00038.html
* NMU non-team-maintained dependencies with RC bugs (dh compat 4) to avoid testing autoremovals of our packages.
* Change repackaging framework from repack.{stub,local} to Files-Excluded for all packages except a couple special cases. Update repacking.pod documentation as well.
* Change (even more) URLs in debian/upstream/metadata to use HTTPS.
* Checked -perl packages affected by the GCC 6 transition (https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=ftbfs-gcc-6;users=debian-gcc@lists.debian.org): everything seems fine or already tracked upstream. And #816571 

Packages / bugs / uploads
-------------------------

* Fix #828181 in libmojolicious-perl in git.
* Update libdatetime-timezone-perl to Olson db 2016e in unstable and stable.
* pkg-perl-tools: examples/check-build: update to work with adt-run and autopkgtest.
* Review half a dozen new packages, upload one, set others to UNRELEASED.
* Review another half dozen new packages, upload most, ask one question.
* Prepare dependencies for new libmessage-passing-zeromq-perl.
* Debug libgtk2-perl FTBFS on powerpc.
* New libio-socket-ssl-perl upload to unstable.
* File and fix #829066, #829064, thanks ci.debian.net.
* Debug libdevel-gdb-perl brittle test a bit (#784845).
* #816571 in libwx-scintilla-perl (gcc-6).
* Fixed a few FTBFS on the reproducible builds infrastructure: libnet-route-perl, XXX

Reproducible builds
-------------------

Made these packages build reproducibly:

* libgoo-canvas-perl
* libgtk2-perl
* libgnome2-perl
* libmarpa-r2-perl
* #828635 in libnet-tclink-perl
* #828636 in libembperl-perl
* latexdiff (#814019)
* libnanomsg-raw-perl

As of 2016-07-01, we're down to a mere 7 team-maintained packages not building
reproducibly.

Tools
-----

* pkg-perl-tools: add update option to dpt-upstream-repo
* .mrconfig: use this new option
* pkg-perl-tools: patchedit: always set Last-Updated to mtime of patch
* upload dh-make-perl: now a native package, split into 2 binary packages: dh-make-perl and libdebian-source-perl.

