=pod

https://wiki.debian.org/Sprints/2015/DebianPerlSprint

=head1 Agenda

- discuss possible src:perl binary package reorganization (DONE)
- ci.debian.net
    * whitelist update http://anonscm.debian.org/cgit/collab-maint/debian-ci-config.git/tree/perl.txt
        + clean up packages that already have a Testsuite header (DONE)
        + check pkg-perl packages not on the whitelist (DONE)
        	671 packages tested, 62 new passing packages added to the whitelist
        	list of work needing packages is available, see http://pkg-perl.alioth.debian.org/autopkgtest.html#ci_debian_net
    * PET integration? -> "patches welcome", filed #786664
    * IRC and/or email notifications about regressions?
    	+ base on http://ci.debian.net/data/status/unstable/amd64/packages.json
- patch handling / debcherry
	+ see #784159
	+ plan: just have a demo (ntyni) and some discussion (DONE)
- dpkg-gencontrol: warning: File::
Lock not available; using flock which is not NFS-safe
	+ see #677865
	+ ntyni has a plan, it just needs implementation
- Andrew Beverley's dh-make-perl patches
	+ see #774068, #774070, #774071, #774072, #774073, #774074
	+ #774068: perllocal.pod handling: change debhelper first, may need a policy change too?
	+ #774073: generate diversions handling code automatically - commented
	+ #774072: looks sane, but needs confirmation and testing - patch applied
	+ #774070: looks sane, but needs confirmation and testing - patch applied
	+ #774071: patch may need a bit of attention but basically OK - patch applied
- other dh-make-perl work
	+ https://anonscm.debian.org/cgit/pkg-perl/packages/dh-make-perl.git/log/
- reproducible builds
    + what to do with POD_MAN_DATE ? (probably implement it in debhelper) see #782879
    + upstreaming patches for perl itself (to wait until after 5.22 release?)
- perl 5.22 preparations
    + upload RC1 to experimental (with binary package reorganization)
    + Module::Build and CGI.pm core removals
    + we need a plan for Test::Tester / Test::use::ok
    	- perl should Break and Replace older (or all?) versions of libtest-tester-perl and libtest-use-ok-perl
    	- but probably not Provide: they aren't separate on CPAN anymore (?)
    	- for separate packages: https://lists.debian.org/debian-perl/2015/04/msg00022.html f.
- versioned provides: needs dose3 support for wanna-build? see https://lists.debian.org/debian-devel/2015/04/msg00246.html
	+ plan for sprint: try them out a bit with sbuild et al.
	+ #786671 filed against dose3
- Shortly discuss moving of lintian checks from pkg-perl-tools to lintian proper
  (if necessary) → Nothing needed to discuss, XTaran has a "go".
- packages with RC bugs / file RM bugs (DONE, for the hopeless 2013/5.18 generation)



=head1 Report


=head2 Intro

7 members of the Debian Perl Group met in Barcelona over the weekend from May 22 to May 24 2015 to kick of the development around perl for stretch and to work on QA tasks across our 3000+ packages.

For the preparation cf. https://wiki.debian.org/Sprints/2015/DebianPerlSprint

The participants would like to thank the Computer Architecture Department of the Universitat Politècnica de Catalunya (UPC) for hosting us, and all donors to the Debian project who helped to cover a large part of our expenses.


=head2 Bugs and uploads

=head3 Overview

Tagged with user:debian-perl@lists.debian.org , usertags: bcn2015

https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=bcn2015;users=debian-perl@lists.debian.org

A total of 53 bugs were filed/worked on. These include:
 - newly filed: 28 bugs
   - incl. 6 fixed during the sprint
   - incl. 8 RM bugs to drop RC-buggy unaintained upstream software
   - incl. 6 bugs on DUCK after running it over all group's packages
 - resolved: 13 bugs
   - incl. 6 bugs filed during the sprint

A total of 31 accepted uploads were made at the sprint.

=head3 Upstream bug reports filed:

* https://github.com/dod38fr/config-model-tk-ui/issues/1 (already fixed)
* https://mail.gnome.org/archives/gtk-perl-list/2015-May/msg00020.html

=head3 FTBFS bugs

- 7 RM bugs filed for packages with FTBFS bugs since perl 5.18 (i.e. 2 years).
- https://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks updated


=head2 git and patches

Followup to pkg-perl BoF at DebConf14.
Currently: storing patches in git with quilt, with the actual source in git unpatched.
This has downsides like having to remember 'quilt push -a' when running 'debian/rules build'
manually from a git checkout. Also, git would be a better tool at rebasing/merging the
patches to new upstream releases, but currently this needs to be done manually with quilt. 
The upside is that it's very simple and straightforward; we need to consider also less
deeply involved team members, and requiring complicated workflows can drive them away. 

=head3 git-debcherry

One tool: git-debcherry (David Bremner), shipped in the gitpkg package.
Missing piece: preserving patch meta-information (DEP3)
  the commits changing upstream code don't get rebased in this workflow, but the metadata is volatile
  git notes solves this neatly: the metadata is attached to commits but can be modified later
git-notes is still a bit rough UI-wise:
 merging is not straightforward if several people work on the same notes at the same time (not expected to be a real problem)
 needs manual pushing/fetching, could probably be integrated into team-specific tools like dpt-push

Niko has submitted a patch for git-debcherry to use git notes for the patch metadata: #784159
Niko showed a demo of the patched git-debcherry + proof-of-concept conversion scripts.
* see http://anonscm.debian.org/cgit/pkg-perl/scripts.git/tree/debcherry, contents:

  git-debcherry-784159
    git-debcherry script from gitpkg_0.26, enhanced to support git notes
    with the patch from https://bugs.debian.org/784159

  quilt2debcherry
    script to convert quilt patches in a package into a commit series
    with the patch metadata stored as git notes

  run-debcherry
    script to automate exporting patches from the commit series and notes,
    by first running git-debcherry itself and then mangling the results
    aiming for stable output with the two scripts below

  notes2dep3
    script to convert "git format-patch" output of commits with patch
    metadata inside git notes into a DEP3-compatible format

  rename-patches
    script to rename patches as created by "git format-patch" into
    names spefied by the "Patch-Name" header stored in the git notes

 Conclusions wrt. git-debcherry: it needs a separate "export" step to keep
 debian/patches synced with the actual git commits, and it's not
 clear how that should fit in a standard workflow. Possibly this is comparable
 to debian/changelog handling with tools like gbp-dch, and it should be the
 responsibility of everybody who works on a package to leave debian/patches
 in a "sane state" afterwards.
 
 Storing patch metadata in git notes looks very interesting and may have
 broader applications than just with git-debcherry.
 
 There's still not much experience in how well git-debcherry works in the long run.

gregoa: integration into git-buildpackage: ~/.gbp.conf:
    [buildpackage]
    prebuild = GIT_DIR=$GBP_GIT_DIR git-debcherry -o debian/patches upstream

Issue: when to export (and commit?) patches; in master, a separate release branch?
=> question for bremner

Issue: needs more thoughts, good documentation, ...

Actions:
* David to review git-debcherry patch.
* Looking at it again at the Rolling Sprint (DebCamp).

=head3 git-dpm

Dom demos git-dpm (as used in src:perl).

git-dpm has exported and applied debian/patches in master;
git-debcherry needs export (so not trivially buildable frm git).


=head2 dpt

dpt (short for Debian Perl Tools) is a wrapper around a couple of helper scripts useful for pkg-perl, and packaged in pkg-perl-tools.

=head3 Importing upstream Git history

Some of us use dpt-import-orig,-upstream-repo,-debian-upstream to import
upstream's Git history into our "upstream" branch, when importing a new upstream
release.

=> It is recommended to do that as well, especially for packages where it has
already been done in the past (to avoid messy history on the "upstream" Git
branch).

=head3 Pushing upstream Git tags to Alioth

Not necessary (no changes to dpt etc. necessary). `dpt push` does not do it
by default.

=head3 Maintaining debian/upstream/metadata

dpt-upstream-metadata creates this file. It is usually only run once,
further changes must be done manually => we have to keep an eye on such
updates when importing new upstream releases.

=head3 Add more dpt-*?

Documentation (git.html) needs to be checked for new dpt-* (as discussed above).
Dam: done reviewing; small corrections here and there made.

=head2 doc review

* Many of the HTML docs on pkg-perl.alioth.debian.org to be reviewed.
* All ancient ones (everything not touched since 2011) already got removed.
* Improve autopkgtest docs with some notes and a new section about setting schroot up (alexm).


=head2 src:perl plans/packaging changes for stretch

Niko presented plans for reorganizing src:perl packaging to achieve co-installability of libperl5.xx (including the standard library) for multiple versions and architectures, and asked for feedback. The ensuing discussion was good and constructive.

See http://people.debian.org/~ntyni/perl/libperl/
Preference for Option S (least packages, with the standard library bundled into libperl5.xx), with perl-modules-*

* Option S most simple, but with some file duplication
* Option L most perfect (no duplication, etc.) but quite complex and maybe hard to maintain properly (lots of break/conflicts/replaces, etc, possible pre-depends fragility)
* The perl package maintainers (ntyni and Dom) prefer Option S due to its simplicity.

Actions:

* ntyni and Dom to move forward with Option S (with perl-modules-5.xx) with Perl 5.22
* upload to experimental (DONE), aimed for sid in a couple of months
* normal rebuild testing (already started) + possibly re-running autopkgtest checks
* announce on -devel (TBD)
* change Perl policy where needed
  * dom drafted - to file bug once upload to experimental is done
     see http://lists.alioth.debian.org/pipermail/perl-maintainers/2015-May/004889.html


=head2 investigating build failures for perl on buildds

* #785557
* seems to relate to the times(2)_system call returning zero for user CPU time

=head2 related to the perl 5.22 release/cleanup

* filed bugs on packages build-depending on "perl5" (will be gone in 5.22) [dom]
  * dh-lisp
  * fte (DONE via QA upload)
  * htag
  * libconvert-units-perl (likely can be put under the pkg-perl umbrella as eloy was fine with similar cases)
  * liblingua-en-words2nums-perl (DONE)
  * libmasonx-request-withapachesession-perl (DONE)
  * librtf-document-perl
  * mimefilter

Test repo: https://people.debian.org/~dom/perl/test/perl-5.22.0/setup.sh{,.asc}


=head2 ci.debian.net

=head3 CI and KGB

IRC notifications on autopkgtest regressions:
Parse http://ci.debian.net/data/status/unstable/amd64/packages.json every day
and report any new failures (previous_status=='pass') via KGB
Only for pkg-perl packages (list retrieved via UDD)
adt-kgb tool idea appeared, reusable by other teams

Dam wrote script/kgb-ci-report in KGB. \o/
It is available so that e.g. other teams
can re-use it.

=head3 Newly autopkgtest-able packages

Some of those were already on the whitelist, some others were fixed so that
they can now be autopkgtest'ed.

- libb-hooks-op-annotation-perl
- libcairo-gobject-perl
- libcairo-perl
- libclutter-perl
- libdevice-cdio-perl
- libgtk3-perl
- libmoox-handlesvia-perl
- libmoox-late-perl
- libmoox-types-mooselike-perl
- libnet-dbus-perl
- libpango-perl
- libparse-debianchangelog-perl

A bunch of other packages got fixed to make their autopkgtests work.

=head3 whitelist updates

ci.debian.net has a whitelist for perl packages which succeeded on on inital manual run.
perl packages are tested via autopkgtest-pkg-perl even of they don't have a Testsuite header.

Cleanup of the ci.debian.net whitelist:
 http://anonscm.debian.org/cgit/collab-maint/debian-ci-config.git/commit/?id=b27313668c16bc77f3d11c027c4fb5fe902c772a

Test run results of non-whitelisted packages:
 http://anonscm.debian.org/cgit/collab-maint/debian-ci-config.git/commit/?id=1421bfbcd1b62e0125bf323cdb33771734c9679a

Updated autopkgtest.pod with the results + a list of work needing packages:
 http://anonscm.debian.org/cgit/pkg-perl/website.git/commit/?id=07a1031986e96974bb55f4d216f1476c9832bc43


=head2 migrations

- Gstreamer 0.10 -> 1.0:  #785856 (would need a new libgstreamer1-perl package, few people care -> have libgstreamer-perl removed and package libgstreamer1-perl later if somebody needs it?) -> bug updated
- OGRE (#732725): low popcon (max 26, now 22), no reverse deps (just one Recommends and one Enhances) = not really needed + seems dead upstream --> let go
- libois-perl possibly makes no more much sense without libogre-perl.


=head2 Recurring Tasks

* Running DUCK over all our packages (abe): https://people.debian.org/~abe/pkg-perl-duck-2015-05-23.txt
  + 6 bug reports filed against duck due to false positives. Already positive feedback from duck's maintainer.
  + Axel managed to go through all non-e-mail issues up to "libf" during the sprint and travelling back, continuing afterwards. Currently still open issue listed in https://people.debian.org/~abe/pkg-perl-duck-2015-05-23.updated.txt
* (Re)subscribe pkg-perl launchpad team to all our packages (gregoa)


=head2 Bugs in Launchpad

There is the group where people can join and then get mails: https://launchpad.net/~pkg-perl-maintainers (JFTR: The launchpad account for the Debian Perl Group is at https://launchpad.net/~pkg-perl-maintainers-lists-alioth)

For the valid-also-for-Debian reports, forwarding to the BTS sounds like the way to go; and/or close them with an upload to debian (with the "LP: #nn" syntax).


=head2 d/changelog handling

After some discussion we discovered that the sensible way is already described in our documentation: "The current group policy states that whenever you stop working on a package, the changelog should be updated and pushed." (https://pkg-perl.alioth.debian.org/git.html#commit_policy)


=head2 libfile-fcntllock-perl

Discussed; conclusions:

- File::FcntlLock (upstream?) needs to fall back to ::Pure
  if ::XS is not available

- the Debian package needs to install ::Pure into a non-version
  specific path as above

- Depends: perlapi-* can be demoted to Recommends

- dpkg-dev can Depend on libfile-fcntllock-perl 

- newer versions of perl may want to Break older versions of libfile-fcntllock-perl
? (but what about the varying binNMU suffixes etc. ?)

#677865 updated, upstream cc'd with the fetaure request about this fallback


=head2 reproducibility and POD_MAN_DATE

commented on #782879 / #782878, waiting for input from debhelper maintainers



=head1 ACCOUNTING

Budget also on https://wiki.debian.org/Sprints/2015/DebianPerlSprint
Procedure https://wiki.debian.org/Teams/DPL/Reimbursement
Approval https://lists.debian.org/debian-sprints/2015/03/msg00001.html

intri put data into ledger, will create a nice balance sheet and open the RT ticket. Merci!


=head1 REPORT

Procedure https://wiki.debian.org/Sprints/HowTo
alexm to coordinate. Gràcies!
Gobby cleanup within a week by everyone.
