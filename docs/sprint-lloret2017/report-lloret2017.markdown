Debian Perl Sprint 2017
=======================

Introduction
------------

4 members of the Debian Perl Group met in Lloret de Mar over the weekend
from May 18 to May 21 2017 as part of the [Debian Sun Camp][] to kick off
the development around perl for Buster and to work on QA tasks across our
3000+ packages. The preparation details can be found on the [sprint wiki][].

The participants would like to thank the [Debian Sun Camp][] organizers for
providing the framework for our sprint, and all donors to the Debian project
who helped to cover a large part of our expenses.

[Debian Sun Camp]: https://wiki.debian.org/DebianEvents/Europe/2017/DSC
[sprint wiki]: https://wiki.debian.org/Sprints/2017/DebianPerlSprint


Bugs and Packages
=================

Overview
--------

Bugs [tagged][] with:

* user: debian-perl@lists.debian.org
* usertags: bcn2017

A total of 23 bugs were filed/worked on. These include:

* newly filed: 4 bugs
    - incl. 3 fixed during the sprint
    - incl. 1 RoM bug to drop unmaintained upstream software
* resolved: 18 bugs
    - incl. 3 bugs filed during the sprint

[tagged]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=bcn2017;users=debian-perl@lists.debian.org

Some details
------------

* [RC bugs in jessie][]: looked at all of them, prepared updates for 8,
  others are probably candidates for `jessie-ignore` or similar
    - [#788008][]: libcgi-application-plugin-anytemplate-perl: missing dependency on libclone-perl
    - [#810655][]: libembperl-perl: postinst fails when libapache2-mod-perl2 is not installed
    - [#783656][]: libhtml-microformats-perl: missing dependency on libmodule-pluggable-perl
    - [#788350][]: FTBFS - proxy tests
    - [#824843][]: libsys-syscall-perl: FTBFS on arm64: test suite failures
    - [#824936][]: libsys-syscall-perl: FTBFS on mips\*: test failures
    - [#808454][]: libdata-faker-perl: FTBFS under some locales (eg. fr\_CH.UTF-8)
    - [#834961][]: libvitacilina-perl: FTBFS too much often (configure fails)
    - [#848060][]: libx11-protocol-other-perl: FTBFS randomly (failing tests)
    - [#849777][]: shutter: CVE-2016-10081: Insecure use of perl exec()
* [#861952][] libcgi-validop-perl: FTBFS on May 1st (?)
    - removal candidate, RM bug filed: [#863073][]
* [#858394][]: ITA: libterm-readline-zoid-perl -- Pure Perl implementation of Readline libraries
    - took over libterm-readline-zoid-perl under pkg-perl umbrella
* [#853045][]: pkg-perl-tools: Broken link for repack.sh in examples/repack.stub
    - verified that broken link is now working
* [#832059][]: pkg-perl-tools: "dpt gen-itp" prints Config::Model warning inside ITP text
    - verified that dh-make-perl now uses https for copyright format URLs


[RC bugs in jessie]: https://lists.alioth.debian.org/pipermail/pkg-perl-maintainers/2017-May/110256.html
[#788008]: https://bugs.debian.org/788008
[#810655]: https://bugs.debian.org/810655
[#783656]: https://bugs.debian.org/783656
[#788350]: https://bugs.debian.org/788350
[#824843]: https://bugs.debian.org/824843
[#824936]: https://bugs.debian.org/824936
[#808454]: https://bugs.debian.org/808454
[#834961]: https://bugs.debian.org/834961
[#848060]: https://bugs.debian.org/848060
[#849777]: https://bugs.debian.org/849777
[#861952]: https://bugs.debian.org/861952
[#863073]: https://bugs.debian.org/863073
[#858394]: https://bugs.debian.org/858394
[#853045]: https://bugs.debian.org/853045
[#832059]: https://bugs.debian.org/832059


Perl 5.26
=========

src:perl
--------

*perl* 5.26.0-RC1 was packaged and uploaded to experimental. Some
notable details include:

* switched to versioned `Provides`, see below
* upgraded [perl.debian.net][] to a new more powerful host for the
  5.26 rebuilds
* started first test rebuilds, see the [Perl-5.26-QA][] gobby document
* updated the transition scripts in the `pkg-perl/scripts` repo
* uploaded fixed *libparams-classify-perl* to experimental,
  *libwx-perl* and *libterm-readline-zoid-perl* to unstable,
  *libdevel-cover-perl* 1.25 to experimental, *libdate-pcalc-perl* 6.1-5
  to experimental
* identified a probable *stretch* to *buster* upgrade issue with
  `debconf` ([#863071][], patch included) that got fixed for *stretch*

[perl.debian.net]: http://perl.debian.net/
[Perl-5.26-QA]: https://gobby.debian.org/export/Teams/Perl/Perl-5.26-QA
[#863071]: https://bugs.debian.org/863071

Versioned Provides
------------------

Deploying versioned provides in *src:perl* would simplify numerous
dependencies. For instance, *perl* could `Provides: libtest-simple-perl
(= 1.xxxxxx)` and other packages could then only `(Build-)Depends:
libtest-simple-perl (>= 1.xxxxxx)`
without needing an alternative dependency on *perl*. See
*debian-policy* bug ([#761219][]).

This was expected to work in
toolchain and infrastructure since *dose3* has a fix in *stretch*
([#786671][]) and *britney* support has been in place for a year
([#786803][]), but there were vague worries about remaining
*wanna-build* (which uses *dose3*) issues.

Versioned provides were deployed in a *src:perl* upload to experimental
([#758100][]), and some TODO items were identified for uploading to
unstable:

* ask on *debian-devel*, ask release team (done after the sprint, see [d-d thread][])
* change reverse dependencies (update `cme` probably) at their own speed
* change/remove `lintian` check (versioned-dependency-satisfied-by-perl)
* upload a couple packages using the new versioned `Provides` to test

More recently, *wanna-build* issues indeed surfaced shortly after *perl*
5.24 upload to unstable with versioned `Provides` enabled ([#867104][] -
fixed on 2017-07-19).  Additionally, an *autopkgtest* problem was spotted
([#867081][]). The change was then (hopefully temporarily) reverted
so that these issues can be fixed and the fixes can be deployed in the
infrastructure.

[#761219]: https://bugs.debian.org/761219
[#786671]: https://bugs.debian.org/786671
[#786803]: https://bugs.debian.org/786803
[#758100]: https://bugs.debian.org/758100
[#867104]: https://bugs.debian.org/867104
[#867081]: https://bugs.debian.org/867081
[d-d thread]: https://lists.debian.org/debian-devel/2017/06/msg00236.html


Insecure loading of YAML data
=============================

Bug [#861958][] reported *lintian: insecure YAML validation* in early
May, which also was tracked later in [CVE-2017-8829][]. A review of all
YAML loaders available in Debian was finished during the sprint (details
can be found in [YAML unsafe][]).

Several approaches were tested in order to find whether they would break
any packages in Debian:

* Test rebuilds on [perl.debian.net][] with the blessing turned off (in
  a couple of different ways) revealed a dozen or so FTBFS regressions.
* Disabling the blessing feature globally is still way too invasive.
  Checking for a `DESTROY` method and inhibit instantiation only then
  seems more appropriate.

The switch to control the behaviour should be an environment variable
since several packages have been written to use any of the available
loaders. Ideally, there should be just one switch that works for all
implementations.

[#861958]: https://bugs.debian.org/861958
[CVE-2017-8829]: https://security-tracker.debian.org/tracker/CVE-2017-8829
[YAML unsafe]: https://gobby.debian.org/export/Teams/Perl/YAML-unsafe

autopkgtest
===========

After a short discussion, recursively run of smoke tests in
*pkg-perl-autopkgtest* was enabled in order to remove the burden of
manually adding the list of test to be run for packages that do not
run `t/*.t` only.

The *libfile-sharedir-par-perl* issues in *autopkgtest* were fixed by
using the `smoke-setup` file instead of `smoke-files` to build `blib`
contents needed by tests.

A walk through the *pkg-perl-autopkgtest* setup was performed with
Tincho in order to help setting *autopkgtest* up for the *pkg-golang*
team. The wording of directory chroot type in [autopkgtest][] was
improved (thanks to Tincho for noticing!) and the details on how to
speed up builds with `eatmydata` in `schroot` were added too. On the
other hand, it is also worth noting that the number of indirection
levels is probably needlessly high since *autodep8* came along.

**Open question:** Is there any difference nowadays between
`Testsuite: autopkgtest` and `Testsuite: autopkgtest-pkg-perl`?

[autopkgtest]: https://pkg-perl.alioth.debian.org/autopkgtest.html

Team Web Pages
==============

The [pkg-perl handbook][] (still a proof of concept) was updated and
locally rebuilt with `sphinx` and `rsync`ed later to alioth (the idea
of building it on alioth was discarded). The details can be found in
[README.sphinx][].

A cleanup of [wiki.d.o Perl pages][] was performed:

* Deleted *Perl/Downloads*.
* Renamed *Perl5.10Transition* to *PerlMaintenance/Perl5.10Transition*.
* Renamed *Perl518TransitionBugs* to
  *PerlMaintenance/Perl518TransitionBugs*.
* Added link to [Debian Perl Group founded][] message in
  *Teams/DebianPerlGroup*.

And some other ideas came up for the future:

* May be interesting to move the content of *PerlBugSquashing* to the
  [pkg-perl handbook][].
* Review pending for *PerlMaintenance/Perl5MajorUpdateProcess* (last
  updated in 2013).

[pkg-perl handbook]: https://pkg-perl.alioth.debian.org/handbook/
[README.sphinx]: https://anonscm.debian.org/cgit/pkg-perl/website.git/tree/README.sphinx
[wiki.d.o Perl pages]: https://wiki.debian.org/FrontPage?action=fullsearch&context=180&value=Perl&titlesearch=Titles
[Debian Perl Group founded]: https://lists.debian.org/debian-devel-announce/2004/01/msg00002.html


Other tasks
===========

* Re-subscribed our Launchpad team [~pkg-perl-maintainers][] to all bugs
  concerning packages we maintain.
* Fixed spelling mistakes in `debian/changelog` in some packages.
* Performed first look into orphaned `lib.*-perl` packages referred by
  the MIA team.
* Discussed the use of `inc/` directories in pkg-perl packages and added
  a summary of the ideas discussed to [OpenTasks][].
* Pushed a [get-ci-failures][] script to the `pkg-perl/scripts` repo;
  this can be used to query [ci.debian.net][] failures.

[~pkg-perl-maintainers]: https://launchpad.net/~pkg-perl-maintainers
[OpenTasks]: https://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks
[get-ci-failures]: https://anonscm.debian.org/cgit/pkg-perl/scripts.git/tree/get-ci-failures
[ci.debian.net]: https://ci.debian.net


Future ideas
============

Paul Wise suggested using Perl::Critic (and maybe other linters like
[all the things][]) to find issues in Perl modules code that could also
be reported upstream. The details were added to *Nice things to have*
section in [OpenTasks][].

We might want to go through [bugs tagged rm-candidate][] and/or add bugs
there to continue removing packages. Also, we might look into orphaned
packages with `O:` in [WNPP list][] (those [mentioned by the MIA team][]
were already there).

[all the things]: https://anonscm.debian.org/cgit/collab-maint/check-all-the-things.git/tree/data/perl.ini
[bugs tagged rm-candidate]: https://udd.debian.org/cgi-bin/bts-usertags.cgi?tag=rm-candidate&user=debian-perl%40lists.debian.org 
[WNPP list]: https://pkg-perl.alioth.debian.org/qa/wnpp.html
[mentioned by the MIA team]: https://lists.debian.org/debian-perl/2017/05/msg00009.html
