Title: Debian Perl Sprint 2017
Slug: debian-perl-sprint-2017
Date: 2017-08-04 12:30
Author: Alex Muntada
Tags: perl, sprint, lloret2017
Status: draft


Four members of the [Debian Perl](https://wiki.debian.org/Teams/DebianPerlGroup) team met in Lloret de Mar over the weekend from May 18 to May 21 2017 as part of the [Debian Sun Camp](https://wiki.debian.org/DebianEvents/Europe/2017/DSC) to kick off the development around perl for Buster and to work on QA tasks across our 3000+ packages.

The participants had a good time and met other Debian friends. The [sprint](https://wiki.debian.org/Sprints/2017/DebianPerlSprint) was productive:

* 23 bugs were filed or worked on, many uploads were accepted.
* The transition to Perl 5.26 was prepared, and versioned provides were deployed in experimental.
* The review of YAML loaders in Debian was finished, and rebuilds without its blessing feature revealed several FTBFS regressions.
* The recursive running of autopkgtests was enabled, and a walk through the team setup was performed.
* Several cleanup tasks were performed, and some ideas were noted for future reference.

The [full report](https://lists.debian.org/debian-perl/2017/08/msg00016.html) was posted to the relevant Debian mailing lists.

The participants would like to thank the Debian Sun Camp organizers for providing the framework for our sprint, and all donors to the Debian project who helped to cover a large part of our expenses.
