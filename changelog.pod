=head1 NAME

changelog - Debian Perl Group changelog header information

=head1 INTRODUCTION

B<PET is not used anymore after the move to salsa.debian.org.>
Maybe the described functionality will be added to
tracker.debian.org at some point. Until then the information
is left for historical reasons.

This is a small guide detailing some common headers that are used in changelog
files. These headers are used by PET (
L<http://pet.debian.net/pkg-perl/pet.cgi>
) in order to properly track the status of the package.

Contributions are welcomed. PET is constantly being updated, and new headers
are still being added. A list of proposed headers is available at
L<http://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks/ChangelogHeaders>

=head1 UNRELEASED

C<UNRELEASED> is not a real header. Until a package is ready for upload, the
distribution is set to C<UNRELEASED> so that everyone is able to quickly see that
this new version of the package has not been uploaded yet. It is also allows
potential sponsors to know that the package is not ready to be uploaded. Once
the package is complete and ready for upload, the distribution should be set to
unstable or whatever distribution the package is intended for.

=head1 IGNORE-VERSION

Sometimes, upstream might release a new version of a package that does not need
to be uploaded in Debian. One reason for this would be if the changes are very
minor, or if the changes do not apply to Debian. C<IGNORE-VERSION> can be used
to make PET aware of this. C<IGNORE-VERSION> takes a version as an argument.

=head2 IGNORE-VERSION Example

C<IGNORE-VERSION: 0.34-1>

This line, if included in a changelog entry, will tell PET that there is no need
to upload the version 0.34-1 of the package.

The debian revision seems required for non-native packages, but
there's no known real-life example where something else than a new
upstream released validates an IGNORE-VERSION tag, so it will be
C<$UPSTREAM_VERSION-1> for nearly every case.

=head1 WAITS-FOR

In some situations, a package might fail to build from source (FTBFS) due to
being unable to satisfy all of its Build-Depends. The C<WAITS-FOR> header can be
used to tell PET that the package is ready, but is waiting on another package to
be uploaded to the archives first. A version can also be passed to C<WAITS-FOR>.
If a version is given, the waited for package shall be of greater or equal
version.

=head2 WAITS-FOR Example

C<WAITS-FOR: foo>

This line will tell PET that this package is ready, but it is waiting on C<foo>
to be uploaded.

C<WAITS-FOR: foo 1.23-4>

This line, like the previous example, will tell PET that this package is ready,
but it is waiting on C<foo> to be uploaded. However, this line will have PET
wait until a version greater than or equal to 1.23-4 of C<foo> is uploaded.

=head1 CONTRIBUTORS

The following people have contributed to the maintainership of this file:

=over

=item * Nathan Handler E<lt>nhandler@ubuntu.comeE<gt>

=back

=head1 LICENSE

Copyright (c) 2009 by the individual authors and contributors noted
above.  All rights reserved. This document is free software; you may
redistribute it and/or modify it under the same terms as Perl itself

Perl is distributed under your choice of the GNU General Public License or the
Artistic License.  On Debian GNU/Linux systems, the complete text of the GNU
General Public License can be found in `/usr/share/common-licenses/GPL' and the
Artistic License in `/usr/share/common-licenses/Artistic'.

=cut
