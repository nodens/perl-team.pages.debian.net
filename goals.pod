=head1 Debian Perl Group

These are our goals:

We want to constantly improve the coverage of available perl modules in debian.
Specifically, we aim to:

=over

=item * Adopt orphaned Perl module packages.

=item * Handle the RFP of Perl modules.

=item * Document and improve the usage of tools like dh-make-perl.

=item * Help with bugs in Perl packages.

=item * Keeping Perl packages in the Debian archive as up-to-date as possible.

=back
