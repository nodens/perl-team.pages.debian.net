=head1 Release Checklist

This is a B<work in progress>.

=head2 Initial Release

=over

=item debian/control

Short description should start with small letter.

Short description should not end with a period.

Short description should read fine when package name and B<is a> are appended
at the beginning of a sentence, e.g. B<foobar is a {short-description}>

Long description should mention the main module name to make
searching easier.  This is often used at the start of the long
description.

debhelper and libmodule-build-tiny-perl need to be in
"Build-Depends", not in "Build-Dependes-Indep", as they are used
during clean.

Run C<cme fix dpkg-control> to remove unneeded versions in deps, etc.

=item debian/changelog

Should only contain the B<Initial Release. (Closes: #NNN)> line for a new package.

=item debian/upstream/metadata

Not nice having B<Contact: unknown>.

Do not blindly use the Copyright holder as Contact.  May have been
abandoned by the original author and someone else is doing the
maintenance and pushing new releases.  Idem for Upstream-Contact in
debian/copyright.

=item debian/patches

Should be forwarded upstream.

=item debian/copyright

Do not blindly use the Copyright holder as Upstream-Contact.  May
have been abandoned by the original author and someone else is doing
the maintenance and pushing new releases.  Idem for Contact in
debian/upstream/metadata.

Should contain information for B<inc/> directories, see L<https://perl-team.pages.debian.net/copyright.html#Module%3A%3AInstall>.
You may need to find out which version of Module::Install is in there, and then
look for the copyright holders in the README of that release.

Should containt the I<Berne convention comment> when the upstream author does not
state the copyright explicitly. See L<https://perl-team.pages.debian.net/copyright.html#Berne_Convention>.

=back

=head2 General

=over

=item packagecheck

    dpt packagecheck -A -C -c

=item licensecheck

    licensecheck --recursive --copyright .

=item cme

    cme fix dpkg-copyright

=item pbuilder

    pdebuild --pbuilder cowbuilder

=item lintian

    lintian -iEvIL +pedantic ../foobar_1.0-1.dsc
    lintian -iEvIL +pedantic ../foobar_1.0-1_amd64.changes
    lintian -iEvIL +pedantic ../foobar_1.0-1_all.deb

=item autopkgtest

    autopkgtest ../foobar_1.0-1.dsc -- schroot sid-amd64-sbuild

=back

=head1 See Also

=over

=item * L<https://perl-team.pages.debian.net/howto/adopt.html>

=back

=cut
